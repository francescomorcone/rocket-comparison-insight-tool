<?php


namespace App\Tests\Service;


use App\Service\Calculator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class CalculatorTest extends WebTestCase
{
    /**
     * @var Calculator
     */
    protected $sut;

    /**
     * @var string
     */
    protected $scoreStr = '{"0":{"score":"2","questionId":"28956310769","categoryId":"28954992357","questionText":"Receiving","categoryText":"Inbound"},"1":{"score":"3","questionId":"28956310780","categoryId":"28954992357","questionText":"Good Receipt Processing","categoryText":"Inbound"},"2":{"score":"1","questionId":"28956310781","categoryId":"28954992357","questionText":"Deconsolidation","categoryText":"Inbound"},"3":{"score":"2","questionId":"28956310782","categoryId":"28954992357","questionText":"Cross-Docking","categoryText":"Inbound"},"4":{"score":"3","questionId":"28954992359","categoryId":"28954992357","questionText":"Unloading","categoryText":"Inbound"},"5":{"score":"1","questionId":"28956310783","categoryId":"28956310774","questionText":"Stocktaking","categoryText":"Internal"},"6":{"score":"2","questionId":"28956310784","categoryId":"28956310774","questionText":"Scrapping","categoryText":"Internal"},"7":{"score":"3","questionId":"28956310785","categoryId":"28956310774","questionText":"Production Integration","categoryText":"Internal"},"8":{"score":"0","questionId":"28956310788","categoryId":"28954992358","questionText":"Warehouse Structure","categoryText":"General"},"9":{"score":"2","questionId":"28956310789","categoryId":"28954992358","questionText":"Graphical Warehouse Layout","categoryText":"General"},"10":{"score":"3","questionId":"28956310790","categoryId":"28954992358","questionText":"Handling Units","categoryText":"General"},"11":{"score":"0","questionId":"28956310792","categoryId":"28954992358","questionText":"Quality Management","categoryText":"General"},"12":{"score":"2","questionId":"28956310793","categoryId":"28954992358","questionText":"Reporting","categoryText":"General"},"13":{"score":"3","questionId":"28956310794","categoryId":"28954992358","questionText":"Resource Management","categoryText":"General"},"14":{"score":"1","questionId":"28954992361","categoryId":"28954992358","questionText":"Transit Warehouse","categoryText":"General"},"15":{"score":"2","questionId":"28956310795","categoryId":"28954992358","questionText":"Labour Management","categoryText":"General"},"16":{"score":"3","questionId":"28954992362","categoryId":"28954992358","questionText":"Yard Management","categoryText":"General"},"17":{"score":"0","questionId":"28954992363","categoryId":"28956310775","questionText":"Wave Management","categoryText":"Outbound"},"18":{"score":"2","questionId":"28954992364","categoryId":"28956310775","questionText":"Kitting","categoryText":"Outbound"},"19":{"score":"3","questionId":"28956310796","categoryId":"28956310775","questionText":"Warehouse Billing","categoryText":"Outbound"},"20":{"score":"0","questionId":"28956310798","categoryId":"28956310775","questionText":"Dock Appointment Scheduling","categoryText":"Outbound"},"21":{"score":"2","questionId":"28956310799","categoryId":"28956310775","questionText":"Goods Issue Processing","categoryText":"Outbound"},"22":{"score":"3","questionId":"28954992365","categoryId":"28956310775","questionText":"Loading","categoryText":"Outbound"},"23":{"score":"0","questionId":"28954992366","categoryId":"28956310775","questionText":"Cartonisation Planning","categoryText":"Outbound"},"24":{"score":"1","questionId":"28956310800","categoryId":"28956310775","questionText":"Value Added Services","categoryText":"Outbound"},"25":{"score":"2","questionId":"28956310801","categoryId":"28956310775","questionText":"Shipping","categoryText":"Outbound"},"26":{"score":"3","questionId":"29111527754","categoryId":"28956310775","questionText":"Returns","categoryText":"Outbound"},"27":{"score":"0","questionId":"29111119703","categoryId":"28956310776","questionText":"Automation Integration","categoryText":"Technology"},"28":{"score":"1","questionId":"29111119704","categoryId":"28956310776","questionText":"Mobile Execution (RF)","categoryText":"Technology"}}';

    public function testSutIsWorking()
    {
        self::bootKernel();
        $container = self::$container;
        $this->sut = $container->get('App\Service\Calculator');

        $this->sut->calculate(json_decode($this->scoreStr));

        print_r($this->sut->result);
    }
}