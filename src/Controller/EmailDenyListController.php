<?php


namespace App\Controller;

use App\Service\HubDb;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EmailDenyListController
 * @package App\Controller
 */
class EmailDenyListController extends AbstractController
{
    /**
     * @var HubDb
     */
    protected $hubDb;

    /**
     * EmailDenyListController constructor.
     * @param HubDb $hubDb
     */
    public function __construct(HubDb $hubDb)
    {
        $this->hubDb = $hubDb;
    }

    /**
     * @Route("/api/denylist", name="denylist_get", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getDenyList()
    {
        return $this->json([$this->hubDb->getDenyList()]);
    }

    /**
     * @Route("/api/denylist/validate/{domain}", name="denylist_validate_get", methods={"GET"})
     *
     * @param  string       $domain
     * @return JsonResponse
     */
    public function validateDomain(string $domain)
    {
        $denyList = $this->hubDb->getDenyList();
        $result = !in_array($domain, $denyList);
        return $this->json(['valid' => $result]);
    }
}