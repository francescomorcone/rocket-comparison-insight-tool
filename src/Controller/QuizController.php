<?php

namespace App\Controller;


use App\Service\HubDb;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class QuizController
 * @package App\Controller
 */
class QuizController extends AbstractController
{
    /**
     * @Route("/quiz", name="quiz")
     *
     * @param  SessionInterface $session
     * @param  HubDb $hubDb
     * @return Response
     */
    public function index(SessionInterface $session, HubDb $hubDb)
    {
        $session->clear();

        return $this->render('quiz/index.html.twig', [
            'questions' => $hubDb->getCompositeQuestions(),
        ]);
    }
}
