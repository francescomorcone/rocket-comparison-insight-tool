<?php

namespace App\Controller;


use App\Service\HubDb;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class CacheController
 * @package App\Controller
 */
class CacheController extends AbstractController
{
    /**
     * @Route("/cache/flush", name="flush")
     *
     * @param  SessionInterface $session
     * @param  HubDb $hubDb
     * @return JsonResponse
     */
    public function flush(SessionInterface $session, HubDb $hubDb)
    {
        $hubDb->getAndCacheCompositeQuestions();
        $hubDb->getAndCacheFormula();
        $hubDb->getAndCacheFunctionalCapability();
        $hubDb->getAndCachePlatformCapability();
        $hubDb->getAndCacheDenyList();

        return new JsonResponse(
            ['status' => 'Success'],
            200
        );
    }
}
