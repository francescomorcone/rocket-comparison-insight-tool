<?php

namespace App\Controller;


use App\Service\HubSpotForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\PdfGenerator;


/**
 * Class ResultprocessController
 * @package App\Controller
 */
class ResultprocessController extends AbstractController
{
    /**
     * @Route("/resultprocess", name="resultprocess")
     *
     * @param  Request $request
     * @param  HubSpotForm $hubSpotForm
     * @param  PdfGenerator $pdfGenerator
     * @return JsonResponse
     */
    public function ajaxCall(
        Request $request,
        PdfGenerator $pdfGenerator,
        HubSpotForm $hubSpotForm
    ) {
        if ($request->isXmlHttpRequest() == false) {
            return new JsonResponse(
                [
                    'status' => 'Error',
                    'message' => 'Error'
                ],
                400
            );
        }

        /**
        * Get data from front end by ajax call
        */
        $userData = $request->get('userData');
        $graphImage = $request->get('graphs');

        $output = $pdfGenerator->getPdf($graphImage, $userData);

        /**
        * Send data to hubspot by hubspot form api
        * Ref: https://developers.hubspot.com/docs/methods/forms/submit_form
        */
        $hubspotutk = '$_COOKIE'; //grab the cookie from the visitors browser.
        $ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
        $hs_context = [
            'hutk' => $hubspotutk,
            'ipAddress' => $ip_addr,
            'pageUrl' => $_SERVER['SERVER_NAME'],
            'pageName' => 'Rocket | Result WMS Comparison Insight Tool'
        ];
        $hs_context_json = json_encode($hs_context);

        //Need to populate these variable with values from the form.
        $str_post = "firstname=" . urlencode($userData['name'])
            . "&lastname=" . urlencode($userData['surname'])
            . "&email=" . urlencode($userData['email'])
            . "&company=" . urlencode($userData['organisation'])
            . "&phone=" . urlencode($userData['phone'])
            . "&sap_wms_selection_tool=" . $this->getParameter('wms.url') . "/" . $output
            . "&hs_context=" . urlencode($hs_context_json); //Leave this one be

        $code = $hubSpotForm->post(
            $str_post, $this->getParameter('wms.forms.result')
        );

        if ($code == 204) {
            return new JsonResponse(
                ['status' => 'ok'],
                200
            );
        }

        return new JsonResponse(
            [
                'status' => 'Error',
                'message' => 'Error',
                'test' => $code
            ],
            (int)$code
        );
    }
}
