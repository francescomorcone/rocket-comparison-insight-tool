<?php


namespace App\Service;


/**
 * Interface CalculatorUnitInterface
 * @package App\Service
 */
interface CalculatorUnitInterface
{
    /**
     * @param $value
     * @param $key
     */
    public function addValue($value, $key);
}