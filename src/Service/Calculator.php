<?php

namespace App\Service;


/**
 * Class Calculator
 * @package App\Service
 */
class Calculator
{    
    /**
     * @var CalculatorScore
     */
    public $met;

    /**
     * @var CalculatorScore
     */
    public $exceeded;

    /**
     * @var CalculatorScore
     */
    public $partially;

    /**
     * @var CalculatorScore
     */
    public $gaps;

    /**
     * @var CalculatorScore
     */
    public $overall;

    /**
     * @var CalculatorUnit
     */
    public $max;

    /**
     * @var string
     */
    public $message = '';

    /**
     * @var int
     */
    public $requirements = 0;

    /**
     * @var array
     */
    public $result = [];

    /**
     * @var array
     */
    public $formula;

    /**
     * @var HubDb
     */
    protected $hubDbRepository;
    
    /**
     * Calculator constructor.
     *
     * @note: for time reason I'm using a factory below.
     *
     * @param HubDb $hubDb
     */
    public function __construct(HubDb $hubDb)
    {
        $this->hubDbRepository = $hubDb;

        $this->met = new CalculatorScore(
            new CalculatorUnit(),
            new CalculatorUnit(),
            new CalculatorUnit(),
            new CalculatorUnit()
        );
        $this->exceeded = new CalculatorScore(
            new CalculatorUnit(),
            new CalculatorUnit(),
            new CalculatorUnit(),
            new CalculatorUnit()
        );
        $this->partially = new CalculatorScore(
            new CalculatorUnit(),
            new CalculatorUnit(),
            new CalculatorUnit(),
            new CalculatorUnit()
        );
        $this->gaps = new CalculatorScore(
            new CalculatorGapsUnit(),
            new CalculatorGapsUnit(),
            new CalculatorGapsUnit(),
            new CalculatorGapsUnit()
        );
        $this->max = new CalculatorUnit();
        $this->message = '';
        $this->requirements = 0;
        $this->result = [];
    }

    /**
     * @param  $userAnswers
     * @return bool
     */
    public function calculate($userAnswers)
    {
        $this->iterateUserAnswers($userAnswers);

        //Selection Score
        $totalRoom = ($this->met->room->total + $this->partially->room->total +
            $this->exceeded->room->total + $this->gaps->room->total);
        $totalRoom = ($totalRoom < 0) ? 0 : $totalRoom;

        $totalWarehouse = ($this->met->warehouse->total + $this->partially->warehouse->total +
            $this->exceeded->warehouse->total + $this->gaps->warehouse->total);
        $totalWarehouse = ($totalWarehouse < 0) ? 0 : $totalWarehouse;

        $totalBasic = ($this->met->basic->total + $this->partially->basic->total +
            $this->exceeded->basic->total + $this->gaps->basic->total);
        $totalBasic = ($totalBasic < 0) ? 0 : $totalBasic;

        $totalAdvanced = ($this->met->advanced->total + $this->partially->advanced->total +
            $this->exceeded->advanced->total + $this->gaps->advanced->total);
        $totalAdvanced = ($totalAdvanced < 0) ? 0 : $totalAdvanced;

        //As Percentage of Max Possible Score
        //(these data are used to create the Rocket WMS Suitability Rating chart)
        $totalRoomPercent = round(100 *  $totalRoom / $this->max->total);
        $totalWarehousePercent = round(100 * $totalWarehouse / $this->max->total);
        $totalBasicPercent = round(100 * $totalBasic / $this->max->total);
        $totalAdvancedPercent = round(100 * $totalAdvanced / $this->max->total);

        $countRoom = ($this->met->room->count + $this->partially->room->count +
            $this->exceeded->room->count + $this->gaps->room->count);
        $countWarehouse = ($this->met->warehouse->count + $this->partially->warehouse->count +
            $this->exceeded->warehouse->count + $this->gaps->warehouse->count);
        $countBasic = ($this->met->basic->count + $this->partially->basic->count +
            $this->exceeded->basic->count + $this->gaps->basic->count);
        $countAdvanced = ($this->met->advanced->count + $this->partially->advanced->count +
            $this->exceeded->advanced->count + $this->gaps->advanced->count);

        ///Platform Requirements Fit Analysis
        $reqRoomMetPercent = $this->met->room->count / $countRoom * 100;
        $reqRoomPartiallyPercent = $this->partially->room->count / $countRoom * 100;;
        $reqRoomExceedPercent = $this->exceeded->room->count / $countRoom * 100;;
        $reqRoomGapsPercent = $this->gaps->room->count / $countRoom * 100;

        $reqWarehouseMetPercent = $this->met->warehouse->count / $countWarehouse * 100;
        $reqWarehousePartiallyPercent = $this->partially->warehouse->count / $countWarehouse * 100;;
        $reqWarehouseExceedPercent = $this->exceeded->warehouse->count / $countWarehouse * 100;;
        $reqWarehouseGapsPercent = $this->gaps->warehouse->count / $countWarehouse * 100;

        $reqBasicMetPercent = $this->met->basic->count / $countBasic * 100;
        $reqBasicPartiallyPercent = $this->partially->basic->count / $countBasic * 100;;
        $reqBasicExceedPercent = $this->exceeded->basic->count / $countBasic * 100;;
        $reqBasicGapsPercent = $this->gaps->basic->count / $countBasic * 100;

        $reqAdvancedMetPercent = $this->met->advanced->count / $countAdvanced * 100;
        $reqAdvancedPartiallyPercent = $this->partially->advanced->count / $countAdvanced * 100;;
        $reqAdvancedExceedPercent = $this->exceeded->advanced->count / $countAdvanced * 100;;
        $reqAdvancedGapsPercent = $this->gaps->advanced->count / $countAdvanced * 100;

        $singleFinalResult = [
            "S/4 Stock Room Management" => $totalRoomPercent,
            "ERP 6.0 Warehouse Management" => $totalWarehousePercent,
            "S/4 EWM Basic" => $totalBasicPercent,
            "S/4 EWM Advanced" => $totalAdvancedPercent,
        ];

        //Requirements Analysis of Number of Requirements
        $this->result = [
            //"MaxPossbileScore" => $MaxPossbileScore,
            "Stock_Room_Management" => round($totalRoom),
            "Warehouse_Management" => round($totalWarehouse),
            "Embedded_EWM_Basic"  =>  round($totalBasic),
            "Embedded_EWM_Advanced" =>  round($totalAdvanced),

            //Rocket WMS Suitability Rating
            "Stock_Room_Management_MaxScore" => $totalRoomPercent,
            "Warehouse_Management_MaxScore" => $totalWarehousePercent,
            "Embedded_EWM_Basic_MaxScore" => $totalBasicPercent,
            "Embedded_EWM_Advanced_MaxScore" => $totalAdvancedPercent,

            "countRoom" => $countRoom,
            "countWarehouse" => $countWarehouse,
            "countBasic" => $countBasic,
            "countAdvanced" => $countAdvanced,

            //Platform Requirements Fit Analysis
            "SRM_Requirements_Met" => number_format($reqRoomMetPercent,2),
            "SRM_Requirements_Partially_Met" => number_format($reqRoomPartiallyPercent,2),
            "SRM_Requirements_Exceeded" => number_format($reqRoomExceedPercent,2),
            "SRM_Gaps" => number_format($reqRoomGapsPercent,2),

            "WM_Requirements_Met" => number_format($reqWarehouseMetPercent,2),
            "WM_Requirements_Partially_Met" => number_format($reqWarehousePartiallyPercent,2),
            "WM_Requirements_Exceeded" => number_format($reqWarehouseExceedPercent,2),
            "WM_Gaps" => number_format($reqWarehouseGapsPercent,2),

            "EB_Requirements_Met" => number_format($reqBasicMetPercent,2),
            "EB_Requirements_Partially_Met" => number_format($reqBasicPartiallyPercent,2),
            "EB_Requirements_Exceeded" => number_format($reqBasicExceedPercent,2),
            "EB_Gaps" => number_format($reqBasicGapsPercent,2),

            "EA_Requirements_Met" => number_format($reqAdvancedMetPercent,2),
            "EA_Requirements_Partially_Met" => number_format($reqAdvancedPartiallyPercent,2),
            "EA_Requirements_Exceeded" => number_format($reqAdvancedExceedPercent,2),
            "EA_Gaps" => number_format($reqAdvancedGapsPercent,2),


            //Calculated OVERALL Fit Score (Capability = Requirement)
            "OverallFitScore_EWMAdvanced" => $this->overall->advanced->array,
            "OverallFitScore_EWMBasic" => $this->overall->basic->array,
            "OverallFitScore_WM" => $this->overall->warehouse->array,
            "OverallFitScore_SRM" =>  $this->overall->room->array,
            "QuestionsAndAnswer" => $userAnswers,

            //Highest Score name
            "HighestScoreName"  => array_keys($singleFinalResult, max($singleFinalResult))[0]
        ];

        $this->message .= "Highest Score " . $this->result['HighestScoreName'] . "\n";

        return true;
    }

    protected function iterateUserAnswers($userAnswers)
    {
        $functional = $this->hubDbRepository->getFunctionalCapability();
        $this->formula = $this->hubDbRepository->getFormula();

        foreach ($userAnswers  as $key => $value) {
            $requirement = $value->score;
            $fRoom = $functional->objects[$key]->values->{3};
            $fWarehouse = $functional->objects[$key]->values->{4};
            $fBasic = $functional->objects[$key]->values->{5};
            $fAdvanced = $functional->objects[$key]->values->{6};

            $this->message .= $value->questionText . " " . $value->score . "\n";

            $this->max->addValue(($requirement * $this->formula['met']['value']), $key);

            // Met Requirements
            // Calculated Fit Score S/4 Stock Room Management
            $this->met->room->addValue(
                (($fRoom == $requirement && $fRoom > 0) ? $requirement * $this->formula['met']['value'] : 0),
                $key
            );
            // Calculated Fit Score ERP 6.0 Warehouse Management
            $this->met->warehouse->addValue(
                (($fWarehouse == $requirement && $fWarehouse > 0) ? $requirement * $this->formula['met']['value'] : 0),
                $key
            );
            // Calculated Fit Score S/4 Embedded EWM Basic
            $this->met->basic->addValue(
                (($fBasic == $requirement && $fBasic > 0) ? $requirement * $this->formula['met']['value'] : 0),
                $key
            );
            // Calculated Fit Score S/4 Embedded EWM Advanced
            $this->met->advanced->addValue(
                (($fAdvanced == $requirement && $fAdvanced > 0) ? $requirement * $this->formula['met']['value'] : 0),
                $key
            );

            // Partial Requirements
            // Calculated Partial Fit Score S/4 Stock Room Management
            $this->partially->room->addValue(
                (($fRoom < $requirement && $fRoom > 0) ? $requirement * $this->formula['partially']['value'] : 0),
                $key
            );
            // Calculated Partial Fit Score ERP 6.0 Warehouse Management
            $this->partially->warehouse->addValue(
                (($fWarehouse < $requirement && $fWarehouse > 0) ? $requirement * $this->formula['partially']['value'] : 0),
                $key
            );
            // Calculated Partial Fit Score S/4 Embedded EWM Basic
            $this->partially->basic->addValue(
                (($fBasic < $requirement && $fBasic > 0) ? $requirement * $this->formula['partially']['value'] : 0),
                $key
            );
            // Calculated Partial Fit Score S/4 Embedded EWM Advanced
            $this->partially->advanced->addValue(
                (($fAdvanced < $requirement && $fAdvanced > 0) ? $requirement * $this->formula['partially']['value'] : 0),
                $key
            );

            // Exceeds Requirements
            // Calculated Exceeds Score S/4 Stock Room Management
            $this->exceeded->room->addValue(
                (($fRoom > $requirement && $fRoom > 0) ? $requirement * $this->formula['exceeded']['value'] : 0),
                $key
            );
            // Calculated Exceeds Score ERP 6.0 Warehouse Management
            $this->exceeded->warehouse->addValue(
                (($fWarehouse > $requirement && $fWarehouse > 0) ? $requirement * $this->formula['exceeded']['value'] : 0),
                $key
            );
            // Calculated Exceeds Score S/4 Embedded EWM Basic
            $this->exceeded->basic->addValue(
                (($fBasic > $requirement && $fBasic > 0) ? $requirement * $this->formula['exceeded']['value'] : 0),
                $key
            );
            // Calculated Exceeds Score S/4 Embedded EWM Advanced
            $this->exceeded->advanced->addValue(
                (($fAdvanced > $requirement && $fAdvanced > 0) ? $requirement * $this->formula['exceeded']['value'] : 0),
                $key
            );

            // Exceeds Requirements
            // Calculated Gap Score S/4 Stock Room Management
            $this->gaps->room->addValue(
                (($fRoom < $requirement && $fRoom == 0) ? $requirement * $this->formula['gaps']['value'] : 0),
                $key
            );
            // Calculated Gap Score ERP 6.0 Warehouse Management
            $this->gaps->warehouse->addValue(
                (($fWarehouse < $requirement && $fWarehouse == 0) ? $requirement * $this->formula['gaps']['value'] : 0),
                $key
            );
            // Calculated Gap Score S/4 Embedded EWM Basic
            $this->gaps->basic->addValue(
                (($fBasic < $requirement && $fBasic == 0) ? $requirement * $this->formula['gaps']['value'] : 0),
                $key
            );
            // Calculated Gap Score S/4 Embedded EWM Advanced
            $this->gaps->advanced->addValue(
                (($fAdvanced < $requirement && $fAdvanced == 0) ? $requirement * $this->formula['gaps']['value'] : 0),
                $key
            );

            // Calculated Overall Defaults
            if ($this->max->array[$key] <= 0) {
                $this->overall->room->array[$key] = "-";
                $this->overall->warehouse->array[$key] = "-";
                $this->overall->basic->array[$key] = "-";
                $this->overall->advanced->array[$key] = "-";

                continue;
            }

            // Calculated Overall Score S/4 Stock Room Management
            $this->overall->room->array[$key] = ($this->met->room->array[$key] > 0)
                ? $this->formula['met']['value']
                : (($this->partially->room->array[$key] > 0)
                    ? $this->formula['partially']['value']
                    : (($this->exceeded->room->array[$key] > 0)
                        ? $this->formula['exceeded']['value']
                        : (($this->gaps->room->array[$key] < 0)
                            ? $this->formula['gaps']['value']
                            : 0
                        )
                    )
                );

            // Calculated Overall Score ERP 6.0 Warehouse Management
            $this->overall->warehouse->array[$key] = ($this->met->warehouse->array[$key] > 0)
                ? $this->formula['met']['value']
                : (($this->partially->warehouse->array[$key] > 0)
                    ? $this->formula['partially']['value']
                    : (($this->exceeded->warehouse->array[$key] > 0)
                        ? $this->formula['exceeded']['value']
                        : (($this->gaps->warehouse->array[$key] < 0)
                            ? $this->formula['gaps']['value']
                            : 0
                        )
                    )
                );

            // Calculated Overall Score S/4 Embedded EWM Basic
            $this->overall->basic->array[$key] = ($this->met->basic->array[$key] > 0)
                ? $this->formula['met']['value']
                : (($this->partially->basic->array[$key] > 0)
                    ? $this->formula['partially']['value']
                    : (($this->exceeded->basic->array[$key] > 0)
                        ? $this->formula['exceeded']['value']
                        : (($this->gaps->basic->array[$key] < 0)
                            ? $this->formula['gaps']['value']
                            : 0
                        )
                    )
                );

            // Calculated Overall Score S/4 Embedded EWM Advanced
            $this->overall->advanced->array[$key] = ($this->met->advanced->array[$key] > 0)
                ? $this->formula['met']['value']
                : (($this->partially->advanced->array[$key] > 0)
                    ? $this->formula['partially']['value']
                    : (($this->exceeded->advanced->array[$key] > 0)
                        ? $this->formula['exceeded']['value']
                        : (($this->gaps->advanced->array[$key] < 0)
                            ? $this->formula['gaps']['value']
                            : 0
                        )
                    )
                );
        }
    }
}
