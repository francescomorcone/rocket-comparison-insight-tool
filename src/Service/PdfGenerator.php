<?php

namespace App\Service;


use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class PdfGenerator
 * @package App\Service
 */
class PdfGenerator
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var HubDb
     */
    protected $hubDb;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Mpdf
     */
    protected $mpdf;

    /**
     * @var ContainerBagInterface
     */
    protected $params;

    /**
     * PdfGenerator constructor.
     *
     * @note: the refactoring of this class is not fully completed for time reason.
     *
     * @param  SessionInterface $session
     * @param  HubDb $hubDb
     * @param  Filesystem $filesystem
     * @param  ContainerBagInterface $params
     * @throws MpdfException
     */
    public function __construct(
        SessionInterface $session,
        HubDb $hubDb,
        Filesystem $filesystem,
        ContainerBagInterface $params
    ) {
        $this->session = $session;
        $this->hubDb = $hubDb;
        $this->filesystem = $filesystem;
        $this->params = $params;

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $this->mpdf = new Mpdf(
            [
                'orientation' => 'L',
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 25,
                'margin_bottom' => 22,
                'margin_header' => 0,
                'margin_footer' => 0,
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../../public/fonts/',
                ]),
                'fontdata' => $fontData + [
                        'roboto' => [
                            'R' => 'Roboto-Regular.ttf',
                            'B' => 'Roboto-Bold.ttf',
                            'I' => 'Roboto-Italic.ttf',
                        ]
                    ],
                'default_font' => 'roboto'
            ]);

        $this->mpdf->useFixedNormalLineHeight = false;
        $this->mpdf->useFixedTextBaseline = false;
        $this->mpdf->adjustFontDescLineheight = 1.0;
        $this->mpdf->shrink_tables_to_fit = 1;
    }
    
    public function getPdf($graphImage, $userData)
    {
//        Create a folder for pdf and one for the Graph
        $folderPdf = $this->params->get('wms.pdf.dir') . "/" . uniqid();
        $this->filesystem->mkdir($folderPdf);
        $formula = $this->hubDb->getFormulaByValue();
        $result = $this->session->get('dataResult');

        $OverallFitScore_EWMAdvanced = $result['OverallFitScore_EWMAdvanced'];
        $OverallFitScore_EWMBasic = $result['OverallFitScore_EWMBasic'];
        $OverallFitScore_WM = $result['OverallFitScore_WM'];
        $OverallFitScore_SRM = $result['OverallFitScore_SRM'];
        $questions = $result['QuestionsAndAnswer'];

        $this->mpdf->SetHTMLHeader(
            '
            <div style="text-align:center; color:#fff; background:#303030; width:100%; padding:30px;">
                 <img src="img/logo.png"/>
            </div>');

        $this->mpdf->SetHTMLFooter('
        
            <div style="width:100%; background:#f2f2f2; padding-top:20px; padding-bottom:20px; padding-left:60px; padding-right:60px;">
                <div style="width:55%; float:left; display: inline-block;">
                    <h3 style="text-align:left;">Personalised Report</h3>                     
                    <div style="width:28%; float:left;">
                        <p style="text-align:left;font-size:18px;">
                          Name: <br>
                          Surname: <br>
                          Organisation: <br>
                          Email: <br>
                          Phone: <br>   
                          Date: <br>  
                        </p>  
                    </div>
                    <div style="width:71%; float:right;">
                        <p style="text-align:left;font-size:18px;">
                          ' . $userData['name'] . ' <br>
                          ' . $userData['surname'] . ' <br>
                          ' . $userData['organisation'] . '<br>
                          ' . $userData['email'] . ' <br>
                          ' . $userData['phone'] . ' <br>   
                          ' . $userData['localDate'] . ' <br>  
                        </p>  
                     </div>
                </div> 
                <div style="width:45%; float:right;">
                    <h3 style="text-align:left; padding-bottom:15px;">Recommendation</h3>                     
                    <p style="text-align:left;font-size:18px;">
                      Based on the information you have provided the most <br>
                      appropriate SAP WMS to meet your Warehouse <br>
                      Management requirements is:<br><br>
                      <p style="color:#EB7328; font-size:20px;">SAP S/4 EWM Advanced</p><br>  
                     </p>  
                </div> 
            </div>
                    
            <div style="background:#303030; width:100%; padding-bottom:15px;">
                <p style="font-family: Roboto, sans-serif; font-weight: 600; text-align:center; color:#fff;  font-size: 18px;"><a href="https://www.rocket-consulting.com" style="color:white; text-decoration: none;">rocket-consulting.com </a><img style="margin-left: 10px; margin-right: 10px; margin-top: 0px; padding-top: 0px; width:9px; height:auto;" src="img/orange-point.svg"/> launch@rocket-consulting.com <img style="margin-left: 10px; margin-right: 10px; margin-top: 0px; padding-top: 0px; width:9px; height:auto;" src="img/orange-point.svg"/> +44 (0)208 334 8026 </p>
            </div>');

        $this->mpdf->WriteHTML('
        <style>  
               
        body { 
        background: radial-gradient(ellipse at center, rgba(91,91,91,1) 1%,rgba(50,50,50,1) 100%);       
        }
        
        td, h3 , div {font-family: Roboto, sans-serif;} 
        .chart_wrap table {display:none;}
        .chart_wrap svg,
        .chart_wrap div {width:100%!important;} 
        
        table#table-result {
          width: 100%;
          background-color: transparent;
          border-collapse: collapse;
          border-width: 1px;
          border-color: #FA7E15;
          border-style: solid;
          color: #FFFFFF;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          margin-left: 48px;
          margin-right: 48px;
        } 

        table#table-result td, table#table-result th {
          border-width: 1px;
          border-color: #FA7E15;
          border-style: solid;
          padding: 15px;
          font-family: Roboto, sans-serif; 
        }
         
        table#table-result thead th {
          background: #FA7E15;
          color:#fff;
          font-size:12px;
          padding-top: 10px;
          padding-bottom: 10px;  
        } 

        table#table-result tr {
          background: transparent;
        }  
        
        table#table-result thead tr th {          
          border-right-color: #3b3b3b;           
          border-right-width: 1px;
          border-right-style: solid;                 
        }
        
         #graps-wrap {
            color:#ffffff;
            background: rgb(91,91,91);
            background: radial-gradient(ellipse at center, rgba(91,91,91,1) 1%,rgba(50,50,50,1) 100%);
        }
                                
        </style>');

        $this->mpdf->WriteHTML(' 
        <div style="width:100%; text-align:center;">
            <img style="height:210px; margin-top: 55px;" src="img/rocket-astronaut.png"/> 
            <div style="width:100%; text-align:center; padding-top:10px;">            
                <h1 style="font-family: roboto, sans-serif; color: white;">Rocket SAP Warehouse <br>Management Selection Tool</h1>
            </div> 
        </div>
        <p style="text-align:center;font-size:18px; margin-top: 0; color: white;">If you would like to gain a fixed price quote for your platform implementation, <a href="https://app.hubspot.com/meetings/caroline-macleod" style="text-align:center; font-size:18px; color: #36AAFF; ">
           please get in touch today.        
        </a></p>  
        ');

        $this->mpdf->SetHTMLHeader('
        <div style="text-align:center; color:#fff; background:#303030; width:100%; padding:10px;">
             <img src="img/logo.png"/>
        </div>');

        $this->mpdf->AddPage();

        $this->mpdf->SetHTMLFooter('
        <div style=" width:100%; background:transparent; text-align:center;">
            <table style="width:75%; padding-top: 20px; padding-bottom: 20px; text-align:center; margin:0 auto;">
                <tbody style="text-align:center;">
                  <tr style="text-align:center;">
                    <td style="font-size:13px; text-align:center; color: white;">
                        <img src="img/green-point.svg" style="width:15px; height:15px; float:left; margin-bottom:-3px; padding-right:10px;"> Requirements met
                    </td>
                    <td style="font-size:13px; text-align:center; color: white;">
                        <img src="img/orange-point.svg" style="width:15px; height:15px; float:left; margin-bottom:-3px; padding-right:10px;"> Requirements partially met
                    </td>
                    <td style="font-size:13px; text-align:center; color: white;">
                        <img src="img/blue-point.svg" style="width:15px; height:15px; float:left; margin-bottom:-3px; padding-right:10px;"> Requirements exceeded
                    </td>
                    <td style="font-size:13px; text-align:center; color: white;">
                        <img src="img/red-point.svg" style="width:15px; height:15px; float:left; margin-bottom:-3px; padding-right:10px; "> Gaps
                    </td>
                  </tr>
                </tbody>
            </table>
        </div>
    
        <div style="background:#303030; width:100%; padding-bottom:18px;">
             <p style="font-family: Roboto, sans-serif; font-weight: 600; text-align:center; color:#fff;  font-size: 18px;"><a href="https://www.rocket-consulting.com" style="color:white; text-decoration: none;">rocket-consulting.com </a><img style="margin-left: 10px; margin-right: 10px; margin-top: 0px; padding-top: 0px; width:9px; height:auto;" src="img/orange-point.svg"/> launch@rocket-consulting.com <img style="margin-left: 10px; margin-right: 10px; margin-top: 0px; padding-top: 0px; width:9px; height:auto;" src="img/orange-point.svg"/> +44 (0)208 334 8026 </p>
        </div>');

        $graphImage['chart_bar'] = str_replace('#ffffff', 'none', $graphImage['chart_bar']);
        $graphImage['chart_bar'] = str_replace('#444444', '#ffffff', $graphImage['chart_bar']);
        $graphImage['chart_bar'] = str_replace('#222222', '#ffffff', $graphImage['chart_bar']);
        $graphImage['chart_bar'] = str_replace('1110', '900', $graphImage['chart_bar']);
        $graphImage['chart_embedded_ewm_advanced'] = preg_replace('/#ffffff/', 'none', $graphImage['chart_embedded_ewm_advanced'],1);
        $graphImage['chart_embedded_ewm_basic'] = preg_replace('/#ffffff/', 'none', $graphImage['chart_embedded_ewm_basic'],1);
        $graphImage['chart_stock_room_management'] = preg_replace('/#ffffff/', 'none', $graphImage['chart_stock_room_management'],1);
        $graphImage['chart_warehouse_management'] = preg_replace('/#ffffff/', 'none', $graphImage['chart_warehouse_management'], 1);

        $this->mpdf->WriteHTML('
        <div style="display:inline-block;">
        <table style="width:100%; padding-top: 25px; padding-bottom: 50px; text-align:center; border:none;">
          <tbody>
             <tr style="text-align:center; width:100%;">      
                <td style="width:100%; text-align:center;">       
                    <h2 style="text-align:center; color: white;">Rocket Suitability Rating </h2>      
                </td>      
            </tr>
            <tr class="result-page">            
                <td>' . $graphImage['chart_bar'] . '</td>
            </tr>             
          </tbody>
        </table >
         <div style="margin-top:-50px;">
            <h2 style="text-align:center; color:white; padding-top:0; padding-bottom: 0;">Requirements Insight </h2> 
         </div>
        <table style="width:100%; border:none; margin-top:20px;">
          <tbody>
            <tr>
              <td style="width:25%; text-align:center;">
              <p style="color:white;">S/4 EWM (Advanced)</p>' . $graphImage['chart_embedded_ewm_advanced'] . '</td>
              <td style="width:25%; text-align:center;">
                <p style="color:white;">S/4 EWM (Basic)</p>' . $graphImage['chart_embedded_ewm_basic'] . '</td>       
              <td style="width:25%; text-align:center;">
                <p style="color:white;">S/4 Stock Room Management</p>' . $graphImage['chart_stock_room_management'] . '</td>
              <td style="width:25%; text-align:center;" > 
                <p style="color:white;">ERP 6.0 Warehouse Management</p>' . $graphImage['chart_warehouse_management'] . '</td>
            </tr>
          </tbody>
        </table>
        </div>');

        $this->mpdf->AddPage();

        $this->mpdf->WriteHTML(' 
        <div style="width:100%; text-align:center;">
            <img style="height:270px; margin-top: 0px;" src="img/rocket-rocketeer-reading-book.png"/> 
            <div style="width:100%; text-align:center;">            
                <h1 style="font-family: roboto, sans-serif; color: white;">Requirements Analysis </h1>
            </div> 
        </div>');

        $this->mpdf->WriteHTML('
        <div style="width:100%; background:#f2f2f2; padding-top:22px; padding-bottom:22px; margin-bottom:20px; padding-left:60px; padding-right:60px;">
            
            <div style="width:100%; text-align:center;">
                <h3 style="text-align:center; padding-bottom:15px;">Recommendation</h3>                     
                <p style="text-align:left; font-size:18px; margin-top:-20px;">
                  This section shows you the detailed analysis of your requirements, showing how each is either fully met, partially met, or exceeded by each WMS, or indeed where there are gaps not addressed by a platform. <br> 
                 </p>  
                 <p style="text-align:left; font-size:18px; margin-top:-10px;">
                 Are you looking for an indicative implementation timeline, budget and resources plan? <a href="https://app.hubspot.com/meetings/caroline-macleod" style="font-size:18px; color: #36AAFF; ">Book a free of charge appointment with one of our experts today.</a>
                 </p> 
            </div> 
        </div>');

        $this->mpdf->AddPage();

        $this->mpdf->WriteHTML(' 
       
        <table style="text-align:center; width:100%; margin:15px;"> 
            <tr style="text-align:center; width:100%; ">
              <td style="width:100%; text-align:center;">
                <h2 style="text-align:center; color: white;">Process Area: ' . $questions->{'0'}->categoryText . '</h2>
              </td>
            </tr>
        </table>       
                   
        <table id="table-result"> 
            <thead> 
                <tr>
                    <th style="width:20%!important;">Functional Requirement</th>
                    <th style="width:20%!important;">Requirement</th> 
                    <th style="width:15%!important;">S/4 EWM (Advanced)</th>
                    <th style="width:15%!important;">S/4 EWM (Basic)</th>                   
                    <th style="width:15%!important;">S/4 Stock Room Management</th>
                    <th style="width:15%!important; border-right: 1px solid #FA7E15 !important;" >ERP 6.0 Warehouse Management</th> 
                </tr>
            </thead> 
        <tbody>');

        $area = $questions->{'0'}->categoryText;
        $len = count($OverallFitScore_EWMBasic);

        foreach ($OverallFitScore_EWMBasic as $key => $value) {

            if ($questions->$key->score == 0) {
                $scroeText = "Not Required";
            } elseif ($questions->$key->score == 1) {
                $scroeText = "Nice to Have / Future";
            } elseif ($questions->$key->score == 2) {
                $scroeText = "Required Simple";
            } elseif ($questions->$key->score == 3) {
                $scroeText = "Required Complex";
            }

            if ($area != $questions->$key->categoryText) {
                $this->mpdf->WriteHTML('   
                </tbody>
              </table> ');
                $this->mpdf->WriteHTML('<pagebreak>');
                $area = $questions->$key->categoryText;
                $this->mpdf->WriteHTML('   

                <table style="text-align:center; width:100%; margin:15px;"> 
                <tr style="text-align:center; width:100%; ">
                  <td style="width:100%; text-align:center;">
                    <h2 style="text-align:center; color: white;">Process Area: ' . $questions->$key->categoryText . '</h2>
                  </td>
                </tr>
              </table>
              <table id="table-result"> 
                <thead> 
                  <tr>
                    <th style="width:20%!important;">Functional Requirement</th>
                    <th style="width:20%!important;">Requirement</th> 
                    <th style="width:15%!important;">S/4 EWM (Advanced)</th>
                     <th style="width:15%!important;">S/4 EWM (Basic)</th>                   
                    <th style="width:15%!important;">S/4 Stock Room Management</th>
                     <th style="width:15%!important; border-right: 1px solid #FA7E15 !important;">ERP 6.0 Warehouse Management</th>  
                  </tr>
                </thead> 
              <tbody>');
            }

            $this->mpdf->WriteHTML('<tr>
              <td style="font-size:13px; text-align:center; color: white;">' . $questions->$key->questionText . '</td>
              <td style="font-size:13px; text-align:center; color: white;">' . $scroeText . '</td>');

            if ($OverallFitScore_EWMAdvanced[$key] == "-") {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">-</td>');
            } else {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">'
                    . '<img src="' . $formula[(string)$OverallFitScore_EWMAdvanced[$key]]['image'] . '" style="width:16px; height:16px;">' .
                    '</td>');
            }

            if ($OverallFitScore_EWMBasic[$key] == "-") {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">-</td>');
            } else {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">'
                    . '<img src="' . $formula[(string)$OverallFitScore_EWMBasic[$key]]['image'] . '" style="width:16px; height:16px;">' .
                    '</td>');
            }

            if ($OverallFitScore_SRM[$key] == "-") {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">-</td>');
            } else {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">'
                    . '<img src="' . $formula[(string)$OverallFitScore_SRM[$key]]['image'] . '"  style="width:16px; height:16px;">' .
                    '</td>');
            }

            if ($OverallFitScore_WM[$key] == "-") {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">-</td>');
            } else {
                $this->mpdf->WriteHTML('<td style="font-size:13px; text-align:center;">' .
                    '<img src="' . $formula[(string)$OverallFitScore_WM[$key]]['image'] . '"  style="width:16px; height:16px;" >' .
                    '</td>');
            }

            $this->mpdf->WriteHTML('</tr> ');

            if ($len - 1 == $key) {
                $this->mpdf->WriteHTML(
                    '</tbody></table>
            <div style="width:100%; text-align: center; margin-top:100px; padding-top:30px; padding-bottom:30px; padding-left:60px; padding-right:60px;">
                     <span class="hs-cta-wrapper" id="hs-cta-wrapper-e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85">
                      <span class="hs-cta-node hs-cta-e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85" id="hs-cta-e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85">
                          <a href="https://cta-redirect.hubspot.com/cta/redirect/3462882/e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85" target="_blank" >
                            <img class="hs-cta-img" id="hs-cta-img-e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/3462882/e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85.png" alt="If you want to increase chances of project success, are not sure about your requirements please book time with an expert today. Arrange a free 1hour session with a consultant to walk you through this process."/>
                          </a>
                      </span>
                      
                      <p style="text-align:center; color: white; font-weight: 600; font-size: 12px;">©Rocket Consulting Ltd 2020. All rights reserved.</p>
                      ');
                $this->mpdf->SetJs("
            var my_awesome_script = document.createElement('script');
            my_awesome_script.setAttribute('src','https://js.hscta.net/cta/current.js');
            document.head.appendChild(my_awesome_script);
        ");
                $this->mpdf->SetJS("hbspt.cta.load(3462882, 'e8a3bf22-6a83-48b1-a3cb-ffe47d30ab85', {});");
                $this->mpdf->WriteHTML('</span></div>');
            }
        }

        $output = $folderPdf . '/' . $this->params->get('wms.pdf.name');

        $this->mpdf->Output($output, 'F');

        return $output;
    }
}
