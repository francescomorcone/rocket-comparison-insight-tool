/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/style.scss');
require('jquery/dist/jquery.min.js');
require('bootstrap');  

$(document).ready(function () {

	//Automate submit form when click
	$('.input').keypress(function (e) {
		if (e.which == 13) {
			$('#send-form').click();
			return false;
		}
	});

	// $('#step-wrap').keypress(function (e) {
	// 	let kCode = e.keyCode || e.which;
	// 	if (kCode == 13) {
	// 		let confirm = $('.confirm-button:visible');
	// 		if (confirm.length) {
	// 			confirm.trigger('click');
	//
	// 		}
	// 		return false;
	// 	}
	// });

	var blackListEmail = [];
	$.get("/api/denylist", function(data) {
		blackListEmail = data[0];
	});


	var checkEmail = true;
	var domainEmail;
	$.validator.addMethod(
		"checkEmailList",
		function() {
			domainEmail = $('#email').val().split('@').pop();
			checkEmail = !blackListEmail.includes(domainEmail);
			if (!checkEmail) {
				$('#checkEmailList-modal').modal('show');
			}
			return checkEmail;
		},
		"Please enter a business email address to proceed with the assessment."
	);

	//Event submit form
	$(document).on("click", "#send-form", function () {
	    //Setting rules to use jquery validate
		$("form").validate({
			rules: {
				name: "required",
				surname: "required",
				email: {
					required: true,
					email: true,
					checkEmailList: true
				},
			},
       		//Success submit form
			submitHandler: function (form) {
				$.get("/api/denylist/validate/" + domainEmail, function(data) {
					if (data.valid) {
						if (checkEmail) {
							//when the form has been completed, modify the screen by removing the classes of the first step and scroll up automaticlay
							$('#step-wrap .steps ul .current').addClass('past, checked');
							$('#step-wrap .steps ul .current').next().find($("a")).click();
							$("body").removeClass('wrap-first-step');
							$('.content').height('auto');
							$('.title-page , .description-message, .steps').show();
							$('html, body').animate({
								scrollTop: 0
							}, 500);

							$(document).on("click", "#step-wrap .content .last-step  .confirm-button span", function () {

								//When click an answer, check if is a last step
								if ($(".last-step").attr("aria-hidden") === 'false') {
									if ($("#step-wrap .content .current input:checked").length) {

										//Loading screen when send data
										$("#loding").fadeIn();
										var allResults = {}
										var countQuestions = $(".question-wrap").length;

										$('.question-wrap').each(function (index) {
											var singleReult = {};

											var valueScore = $(this).find('input[type="radio"]:checked').attr("value");
											//singleReult['score'] = valueScore;
											singleReult.score = valueScore;

											var questionId = $(this).attr("data-question-id");
											//singleReult['questionId'] = questionId;
											singleReult.questionId = questionId;

											var categoryId = $(this).attr("data-cat-id");
											//singleReult['categoryId'] = categoryId;
											singleReult.categoryId = categoryId;

											var questionText = $(this).find(".question").text();
											singleReult.questionText = questionText;

											var categoryText = $(this).attr("data-cat");
											singleReult.categoryText = categoryText;

											//allResults.push( JSON.stringify( singleReult ) );
											allResults[index] = singleReult
											if (index + 1 === countQuestions) {
												sendData();
											}
										});

										function sendData() {
											$.ajax({
												url: '/dataprocess',
												cache: false,
												data: {
													"userData": JSON.stringify({
														name: $("#wrap-form #name").val(),
														surname: $("#wrap-form #surname").val(),
														company: $("#wrap-form #company").val(),
														email: $("#wrap-form #email").val(),
														telephone: $("#wrap-form #telephone").val()
													}),
													"userScore": JSON.stringify(allResults),
												},
												type: 'post',
												success: function (output) {
													window.location.replace('result');
												}
											});
										}
									}
								}
							});
						}
					}
				});
			}
		});
	});

})
