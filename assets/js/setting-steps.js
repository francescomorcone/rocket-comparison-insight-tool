require('jquery/dist/jquery.min.js');

//Build slide step
$(document).ready(function () {
 
	var wizard =  $("#step-wrap").steps({
		headerTag: false,
		bodyTag: "section",
		transitionEffect: "slideLeft",
		autoFocus: false,
		enableAllSteps: true,
		enablePagination: false,
		suppressPaginationOnFocus: false,
	}); 
 
	setTimeout(function () {
		$("#pre-loding").fadeOut(); 
	}, 1000);

	//check if is a first step
	firstStep();

	//add class last step
	$("#step-wrap .content section").last().addClass('last-step');


	//move steps block steps
	$( ".steps" ).insertAfter( $( "#step-wrap > .content" ) );
	 

	function updateCounter() {
		$("#error-answer").remove();
		$(".to-show").remove();
		var numberIndex = $("#step-wrap .content .current").attr("data-index");
		$("#number-count span").text(Number(numberIndex));
	}

	//check first step
	function firstStep() {
		if ($(".first-step").attr("aria-hidden") === 'false') {
			$("body").addClass('wrap-first-step');
			$('.title-page , .description-message, .steps').hide();
		}
	}

	//Cleaner dotter
	$('#step-wrap .steps ul li a').empty();

	//counter step part
	var number = $("#step-wrap .content .body").length -1 
	$('#step-wrap .steps ul').after('<div id="number-count"> <div id="arrow-prev"></div> <span>0</span> of ' + number + ' <div id="arrow-next"></div></div>');

  
	//Confirmation button to go in the next step
	$(document).on("click", "#step-wrap .content .current .confirm-button", function () {  
		$("#error-answer").remove();
		if( $( "#step-wrap .content .current input:checked").length ){   
			$('#step-wrap .steps ul .current').addClass('past checked');
			$('#step-wrap .steps ul .current').next().find($("a")).click(); 
			updateCounter();
			firstStep();
		}else{ 
			$("#error-answer").remove();
			$( '<p id="error-answer" class="text-center">Select an answer to continue</p>' ).insertAfter( "#step-wrap .content .current .question-header" ); 
		}  
	});


	//Show answer text  
	$(document).on("click", ".question-wrap .radio-label", function () {
		var toShow = $(this).attr("for");  
		if(toShow[9] == "0" || toShow[9] == "1" || toShow[10] == "1" || toShow[10] == "0" ){     
			$(".question-wrap.current .to-show").remove();
			$(".to-show").val("");
		}else{    
			$(".question-wrap.current .to-show").remove();
			var currentText = $(".question-wrap.current").find("[data-rel='" + toShow  + "']").text();
			$(".question-wrap.current .answer-text").append( '<p class="to-show">'+currentText+'</p>' ); 
		} 
	});

	//controll mobile arrow 
	$(document).on("click", "#arrow-prev", function () {
		$("#step-wrap").steps("previous",{});
	});

	$(document).on("click", "#arrow-next", function () {
		$("#step-wrap").steps("next",{});
	});
 
	//select active dotter
	$(document).on("click", "#step-wrap .steps ul li a", function () {  
		updateCounter();
		$(this).parent().prevAll().addClass('past');
		$(this).parent().nextAll().removeClass('past');
		firstStep();
	}); 

})
 