require('jquery/dist/jquery.min.js');
require('bootstrap');

$(document).ready(function () {
    $(document).on("click", ".active .next", function (event) {
        event.preventDefault();
        var next = $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
        if (next.length == 0) {
            $('.nav-tabs li').first().find('a').trigger('click');
        }
    });
});

var blackListEmail = [];
$.get("/api/denylist", function(data) {
    blackListEmail = data[0];
});

var checkEmail = true;
var domainEmail;
$.validator.addMethod(
    "checkEmailList",
    function () {
        domainEmail = $('#email').val().split('@').pop();
        checkEmail = !blackListEmail.includes(domainEmail);
        return checkEmail;
    },
    "Please enter a business email address to proceed with the assessment."
);

$(document).on("click", "#send-form", function () {

    $("#wrap-form").validate({
        rules: {
            name: "required",
            surname: "required",
            company: "required",
            email: {
                required: true,
                email: true,
                checkEmailList: true
            },
            telephone: {
                required: true,
                min: 9
            },
        },
        //Success submit form
        submitHandler: function (form) {
            $.get("/api/denylist/validate/" + domainEmail, function (data) {
                if (data.valid) {
                    $("#loding").fadeIn();
                    $.ajax({
                        url: '/resultprocess',
                        data: {
                            userData: {
                                localDate: function () {
                                    let newDate = new Date() + '';
                                    let time = new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
                                    let d = newDate.split(' ');
                                    return d[2] + ' ' + d[1] + ' ' + d[3] + ' - ' + time;
                                },
                                name: $("#name").val().trim(),
                                surname: $("#surname").val().trim(),
                                organisation: $("#organisation").val().trim(),
                                email: $("#email").val().trim(),
                                phone: $("#phone").val().trim(),
                            },
                            graphs: {
                                chart_bar: $('#chart_bar svg').prop('outerHTML').replace("position: absolute;", " "),
                                chart_embedded_ewm_advanced: $('#chart_embedded_ewm_advanced svg').prop('outerHTML').replace("position: absolute;", " "),
                                chart_embedded_ewm_basic: $('#chart_embedded_ewm_basic svg').prop('outerHTML').replace("position: absolute;", " "),
                                chart_stock_room_management: $('#chart_stock_room_management svg').prop('outerHTML').replace("position: absolute;", " "),
                                chart_warehouse_management: $('#chart_warehouse_management svg').prop('outerHTML').replace("position: absolute;", " "),
                            }
                        },
                        type: 'post',
                        success: function (output) {
                            if (output.status == "ok") {
                                $("#loding").fadeOut();
                                $("#download-modal").modal('hide');
                                $("#alert-success").show();
                                setTimeout(function () {
                                    $("#alert-success").fadeOut();
                                }, 3000);
                            }
                        }
                    });
                }
            });
        }
    });
});

 

 
